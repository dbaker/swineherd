README
======

Swineherd is a test runner. Its primary purpose is to run piglit_ tests, but
there isn't a reason that it couldn't be used to run other types of tests.


Usage
-----

Swineherd is built using meson, it has a few external dependencies, but does
not build any shared libraries, and can be run from the build directory. It has
a standard -h/--help message that describes the options it supports.

You will need a piglit build directory with up to date .xml.gz files for
swineherd to consume.


Why
---

So why a separate runner for piglit tests? Well, for one I wanted to write
something in C++11 (I ended up using 14) and get my feet a bit wet. For another
there has long been a theory that the piglit runner is slow because of python.
I also wanted a chance to build a runner without some of the design mistakes
that we made in piglit.

Why not put this in piglit itself? Honestly? I hate cmake, particularly
piglit's cmake. It's a terrible pain to work with and getting this up and
running with meson was much easier. It also **requires** C++14, which piglit does
not. 


What makes swineherd different?
-------------------------------

The biggest difference is the way that threading works, in swineherd there is a
thread for the logger, a thread that reads XML, and one or more threads that run
tests. This allows much more work to be done in parallel, and allows finer
tuning of performance by controlling the number of threads. Swineherd also
doesn't make the mistake piglit did of having a "mixed" mode, "serial" mode,
and "concurrent" mode; the number of threads is controlled by the -j option,
just like make or ninja.


What does it not do?
--------------------

A lot of things. Have a look at the issues_ page for some of them. The most
notable things missing from swineherd compared to piglit are support for OSes
other than Linux and FreeBSD (thanks Scott!), though it probably could be made
to work on other BSD OSes pretty easily). There are open issues for this,
patches are welcome. It also doesn't have junit backend support currently.
I'm also planning to fix that.

There are other features I'm not planning to implement, and which I don't
really want patches for. Among them the --dmesg support and the summary tools.
Swineherd is primarily focused on being fast, --dmesg is not fast. Python is
still a nicer language than C++ for doing a lot of string manipulation, which
is what the summary tools do.


Contributing
------------

Contributions are welcome!

Please create a `merge request`_, be sure to add a ``Fixes #`` annotation if it
fixes an opened issue.

Bug reports are also welcome, see the issues_ page.


C++ Style
^^^^^^^^^

Swineherd is written in C++14. Header files should all have an ``.hpp``
extension, and code files should have a ``.cpp`` extension.

Braces are cuddled, types and qualifiers go on their own line in code files, on
the same line in headers:

.. code:: c++

    void
    doThing(const Thing & t) {
        t.method(1);
    }

.. code:: c++

    void doThing(const Thing &);

Use an editorconfig plugin to get proper indent style (4 spaces, no tabs).

Function names are studlyCaps, clases are CamelCaps, variables are underscore_separated.

Prefer references and smart pointers (unique_ptr and shared_ptr) to raw pointers.

Optional braces (such as in one line loops or if statements) are always used:

.. code:: c++

    for (const auto v & : sequence) {
        std::cout << v;
    }

.. code:: c++

    if (mybool) {
        // WaT?
        abort();
    }


Meson Style
^^^^^^^^^^^

Use editorconfig to get proper indentation, 2 spaces no tabs.

Variables are underscore_separated with no caps.

Functions that fit on one line (~80 characters or less) should do so, those
that don't should have each argument on a new line, with the closing brace on
it's own line. The same for lists:

.. code:: meson

    lib = library('lib', 'lib.c')

    exe = executable(
      'exe',
      [
          'source1.cpp', 'source2.cpp', 'source3.cpp', 'source4.cpp', source5.cpp',
          'source6.cpp'
      ],
      link_with : lib,
    )

.. _merge request: https://gitlab.freedesktop.org/dbaker/swineherd/merge_requests
.. _issues: https://gitlab.freedesktop.org/dbaker/swineherd/issues
.. _piglit: https://gitlab.freedesktop.org/mesa/piglit
