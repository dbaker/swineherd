#!/usr/bin/env python3
# Copyright © 2020 Intel Corporation
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Test harness.

"""

import argparse
import gzip
import json
import os
import pathlib
import subprocess
import sys
import tempfile


def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument('swineherd', help='swineherd binary to run')
    parser.add_argument('xml', help='xml profile to test')
    parser.add_argument('expected', help='expected json value')
    args = parser.parse_args()

    with tempfile.TemporaryDirectory() as d:
        p = subprocess.run([args.swineherd, d, 'name', args.xml],
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE,
                           check=True)

        with gzip.open(pathlib.Path(d).joinpath('results.json.gz')) as f:
            result = json.load(f)

    with pathlib.Path(args.expected).open('r') as f:
        expected = json.load(f)

    result = result['tests']
    expected = expected['tests']

    for v in result.values():
        # We need to delete time, because it will never be accurate
        del v['time']

        # We need to normalize the command, removing the path apart from the
        # source directory structure
        cmd = v['command'].split()
        bin_ = cmd[0].split(os.path.sep)
        # Yes, we want to use / to make cross platform work better
        cmd[0] = '/'.join(bin_[-3:])

        # If we have a shder-runner type test, do the same thing for the shader file
        if not cmd[1].startswith('-'):
            bin_ = cmd[1].split()
            bin_ = cmd[1].split(os.path.sep)
            cmd[1] = '/'.join(bin_[-2:])

        v['command'] = ' '.join(cmd)

    if result == expected:
        return 0

    keys = set(result.keys()) | set(expected.keys())
    for k in sorted(keys):
        if result.get(k) != expected.get(k):
            print('Test: ', k)
            print('exepected:', expected.get(k), file=sys.stderr)
            print('acutal:', result.get(k), file=sys.stderr)

    return 1

if __name__ == "__main__":
    sys.exit(main())