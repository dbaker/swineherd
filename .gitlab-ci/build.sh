#!/usr/bin/env bash

set -e
set -o pipefail

meson _builddir $CONFIGURE_OPTIONS -Dbuildtype=debug
ninja -C _builddir
ninja -C _builddir test