/* Copyright © 2018 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "config.h"
#include "result.hpp"

namespace swineherd {

Status
Result::getStatus() {
    Status local = Status::NOTRUN;
    for (auto const & kv : status) {
        if (kv.second > local) {
            local = kv.second;
        }
    }
    return local;
}

Status
strToStatus(std::string * str) {
    if (*str == "pass") {
        return Status::PASS;
    } else if (*str == "warn") {
        return Status::WARN;
    } else if (*str == "fail") {
        return Status::FAIL;
    } else if (*str == "crash") {
        return Status::CRASH;
    } else if (*str == "skip") {
        return Status::SKIP;
    } else if (*str == "notrun") {
        return Status::NOTRUN;
    } else if (*str == "timeout") {
        return Status::TIMEOUT;
    } else {
        UNREACHABLE("Unknown test status " + *str);
    }
}

std::string
to_string(Status status) {
    switch (status) {
    case Status::PASS:
        return "pass";
    case Status::WARN:
        return "warn";
    case Status::FAIL:
        return "fail";
    case Status::CRASH:
        return "crash";
    case Status::SKIP:
        return "skip";
    case Status::NOTRUN:
        return "notrun";
    case Status::TIMEOUT:
        return "timeout";
    default:
        UNREACHABLE("Uknown status");
    };
}

}
