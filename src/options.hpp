/* Copyright © 2018 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <vector>
#include <string>
#include <exception>
#include <regex>

namespace swineherd {

class OptionException : public std::exception {
public:
    OptionException(const std::string message) : msg{message} {};
    const char * what() const throw() {
        return msg.c_str();
    }
private:
    const std::string msg;
};

class Regex {
public:
    Regex(std::string s, std::regex r) : str{s}, regex{r} {};
    ~Regex() {};

    std::string str;
    std::regex regex;
};

class Options {
public:
    Options() {};
    ~Options() {};
    void parse_args(int argc, char ** argv);

    short threads = 1;
    std::vector<std::string> profiles;
    std::string resultdir;
    std::string name;
    std::string platform = "";
    int timeout = -1;
    std::vector<Regex> include_filters;
    std::vector<Regex> exclude_filters;
};

}
