/* Copyright © 2018-2019 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <deque>
#include <memory>
#include <mutex>
#include <condition_variable>

namespace swineherd {

template <class T>
class Queue {
public:
    Queue() {};
    ~Queue() {};

    void put(const T & thing) {
        {
            std::lock_guard<std::mutex> lk(lock);
            if (is_closed) {
                throw std::logic_error("Tried to put on a closed queue.");
            }
            queue.push_back(thing);
        }
        cv.notify_one();
    };

    void put(T && thing) {
        {
            std::lock_guard<std::mutex> lk(lock);
            if (is_closed) {
                throw std::logic_error("Tried to put on a closed queue.");
            }
            queue.push_back(std::move(thing));
        }
        cv.notify_one();
    };

    bool get(T & thing) {
        std::unique_lock<std::mutex> lk(lock);
        cv.wait(lk, [&]{ return is_closed or !queue.empty(); });
        if (queue.empty()) {
            return false;
        }
        std::swap(thing, queue.front());
        queue.pop_front();
        return true;
    };

    void close() {
        // Required in case someone is trying to put something on the queue when we close it
        {
            std::lock_guard<std::mutex> lk(lock);
            is_closed = true;
        }
        cv.notify_all();  // flush any reamining threads.
    };

    bool closed() {
        // Avoid races with the close method
        std::lock_guard<std::mutex> lk(lock);
        return bool(is_closed);
    };

private:
    std::mutex lock;
    std::condition_variable cv;
    std::deque<T> queue;
    bool is_closed = false;
};

}
