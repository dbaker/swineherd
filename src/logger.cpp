/* Copyright © 2018-2019 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <map>
#include <iostream>
#include "logger.hpp"

namespace swineherd {

void
simpleLogger(std::shared_ptr<logQueue> queue, std::unique_ptr<unsigned long> count) {
    std::map<Status, unsigned long> mapping {};
    unsigned long completed = 0;
    Status status;

    while (queue->get(status)) {
        ++completed;
        ++mapping[status];
        std::cout << "[" << completed << "/" << *count << "]";
        for (auto iter = mapping.begin(); iter != mapping.end(); ++iter) {
            std::cout << " " << to_string(iter->first) << ": " << iter->second;
            if (iter != --mapping.end()) {
                std::cout << ",";
            }
        }
        std::cout << '\r' << std::flush;
    };
    std::cout << std::endl;
}

}
