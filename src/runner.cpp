/* Copyright © 2018 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "runner.hpp"
#include "result.hpp"
#include "logger.hpp"
#include "time.hpp"

namespace swineherd {

void
runTests(std::shared_ptr<testQueue> queue, 
         std::shared_ptr<backend::BackendInterface> backend,
         std::shared_ptr<logQueue> log,
         const Options & options,
         const test::SystemInfo & info) {
    testPair pair;

    while (queue->get(pair)) {
        auto name = std::move(std::get<0>(pair));
        auto test = std::move(std::get<1>(pair));

        RunTime time{};
        time.recordStart();
        std::unique_ptr<Result> result = test->runTest(options, info);
        time.recordStop();
        result->time = time;

        log->put(result->getStatus());
        backend->writeTest(*name, *result);
    }
}

}
