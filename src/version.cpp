/* Copyright © 2018 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <vector>
#include <climits>
#include <cassert>

#include "utils.hpp"
#include "version.hpp"

namespace swineherd { 

bool
operator==(const Version & v1, const Version & v2) {
    return (v1.major == v2.major and v1.minor == v2.minor);
}

bool
operator!=(const Version & v1, const Version & v2) {
    return (v1.major != v2.major or v1.minor != v2.minor);
}

bool
operator<=(const Version & v1, const Version & v2) {
    return (v1.major <= v2.major and v1.minor <= v2.minor);
}

bool
operator<(const Version & v1, const Version & v2) {
    return (v1.major < v2.major and v1.minor < v2.minor);
}

bool
operator>=(const Version & v1, const Version & v2) {
    return (v1.major >= v2.major and v1.minor >= v2.minor);
}

bool
operator>(const Version & v1, const Version & v2) {
    return (v1.major > v2.major and v1.minor > v2.minor);
}

Version::operator bool() const {
    return (major > 0 and minor > 0);
}

Version::operator std::string() const {
    return std::to_string(major) + "." + std::to_string(minor);
}

Version::Version(const std::string & str) {
    std::vector<std::string> v{};
    utils::split(str, '.', std::back_inserter(v));
    auto _major = std::stoul(v[0]);
    auto _minor = std::stoul(v[1]);
    assert(_major < USHRT_MAX);
    assert(_minor < USHRT_MAX);
    major = (unsigned short)_major;
    minor = (unsigned short)_minor;
}

}
