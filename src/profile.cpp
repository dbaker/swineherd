/* Copyright © 2018-2019 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <memory>
#include <fstream>
#include <sstream>
#include <string>
#include <array>
#include <cassert>
#include <cstdlib>

#include "config.h"
#include "profile.hpp"
#include "runner.hpp"
#include "group.hpp"
#include "tests/base.hpp"
#include "tests/piglit.hpp"
#include "tests/piglit_waffle.hpp"
#include "utils.hpp"
#include "zstr.hpp"

extern "C" char **environ;

namespace swineherd {

namespace {

enum class State {
    TESTLIST,
    TEST,
    OPTION,
};

State
strToState(const char * raw) {
    std::string state(raw);
    if (state == "PiglitTestList") {
        return State::TESTLIST;
    } else if (state == "Test") {
        return State::TEST;
    } else if (state == "option") {
        return State::OPTION;
    } else {
        UNREACHABLE("Unknown piglit xml type " + state);
    }
}

enum class Type {
    PIGLIT_GL,
    PIGLIT_GL_BUILTIN_CONSTANTS,
    GLSL_PARSER,
    ASM_PARSER,
    SHADER_RUNNER,
    VK_RUNNER,
};

Type
strToType(const char * raw) {
    std::string state(raw);
    if (state == "gl") {
        return Type::PIGLIT_GL;
    } else if (state == "gl_builtin") {
        return Type::PIGLIT_GL_BUILTIN_CONSTANTS;
    } else if (state == "glsl_parser") {
        return Type::GLSL_PARSER;
    } else if (state == "asm_parser") {
        return Type::ASM_PARSER;
    } else if (state == "shader") {
        return Type::SHADER_RUNNER;
    } else if (state == "vkrunner") {
        return Type::VK_RUNNER;
    } else {
        UNREACHABLE("Unknown test type " + state);
    }
}

struct TestData {
    Group name;
    Type type;
    std::vector<std::string> command;
    bool concurrent;
    test::piglit::Profile profile = test::piglit::Profile::COMPAT;
    std::array<unsigned short, 2> shader_version{0, 0};
    std::array<unsigned short, 2> api_version{0, 0};
    std::unordered_set<std::string> extensions;
    std::string asm_type;
    std::string filename;
    std::vector<std::string> require_platforms;
    std::vector<std::string> exclude_platforms;
};

std::vector<std::string>
pythonSequence(std::string value) {
    bool start = false;
    std::vector<std::string> out;
    std::string current;
    for (auto const & c : value.substr(1, -1)) {
        if (c == '\'') {
            start = !start;
            if (!start) {
                out.push_back(std::string(current));
            }
            current.clear();
        } else if (c != ' ') {
            current += c;
        }
    }
    return out;
}

std::string
pythonStr(std::string value) {
    return value.substr(1, value.size() - 2);
}

std::unordered_set<std::string>
pythonSet(std::string value) {
    bool start = false;
    std::unordered_set<std::string> out;
    std::string current;
    for (auto const & c : value.substr(1, -1)) {
        if (c == '\'') {
            start = !start;
            if (!start) {
                out.insert(std::string(current));
            }
            current.clear();
        } else if (c != ' ') {
            current += c;
        }
    }
    return out;
}

std::array<unsigned short, 2>
parseVersion(const std::string value) {
    std::vector<std::string> split{};
    utils::split(value, '.', std::back_inserter(split));
    auto major = std::stoul(split[0]);
    auto minor = std::stoul(split[1]);
    assert(major < USHRT_MAX);
    assert(minor < USHRT_MAX);
    std::array<unsigned short, 2> v{(unsigned short)major, (unsigned short)minor};
    return v;
}

struct ParseState {
    std::shared_ptr<testQueue> queue;
    std::string root;
    std::string name;
    std::map<std::string, std::string> env;
    const Options & opts;
    TestData test;
};

struct Counter {
    Counter(const Options & o) : count{0}, opts{o} {};
    ~Counter() {};

    unsigned long count;
    const Options & opts;
};

void
countStartElementHandler(void * data, const char * name, const char **attrs) {
    State stage = strToState(name);
    auto counter = (Counter *)data;

    switch (stage) {
    case State::TESTLIST:
        if (counter->opts.include_filters.empty() and counter->opts.exclude_filters.empty()) {
            for (unsigned i = 0; attrs[i]; i++) {
                std::string key(attrs[i]);
                ++i;
                if (key == "count") {
                    counter->count += std::stoul(attrs[i]);
                }
            }
        }
        break;
    case State::TEST:
        if (!counter->opts.include_filters.empty() or !counter->opts.exclude_filters.empty()) {
            std::string n{};
            for (unsigned i = 0; attrs[i]; i++) {
                std::string key(attrs[i]);
                ++i;
                if (key == "name") {
                    n = attrs[i];;
                }
            }
            assert(n != "");

            bool valid = true;
            for (auto const & r : counter->opts.exclude_filters) {
                valid = !std::regex_search(n, r.regex);
                if (!valid) {
                    break;
                }
            }

            if (valid) {
                for (auto const & r : counter->opts.include_filters) {
                    valid = std::regex_search(n, r.regex);
                    if (!valid) {
                        break;
                    }
                }
            }

            if (valid) {
                counter->count++;
            }
        }
        break;
    case State::OPTION:
        break;
    }
}

void
countEndElementHandler(void * data UNUSED, const char * name UNUSED) {
    return;
}

void
startElementHandler(void * data, const char * name, const char **attrs) {
    ParseState * state = (ParseState *)data;
    State stage = strToState(name);

    switch(stage) {
    case State::TESTLIST: {
        for (unsigned i = 0; attrs[i]; i++) {
            std::string key(attrs[i]);
            ++i;
            if (key == "name") {
                state->name = attrs[i];
            }
        }
        break;
    }
    case State::TEST: {
        for (unsigned i = 0; attrs[i]; i++) {
            std::string key(attrs[i]);
            ++i;
            if (key == "name") {
                state->test.name = Group(attrs[i]);
            } else if (key == "type") {
                state->test.type = strToType(attrs[i]);
            } else {
                UNREACHABLE(std::string{"Unknown test attribute "} + attrs[i]);
            }
        }
        break;
    }
    case State::OPTION: {
        std::string name(attrs[1]);
        std::string value(attrs[3]);
        if (name == "run_concurrent") {
            state->test.concurrent = value == "True";
        } else if (name == "command") {
            state->test.command = pythonSequence(value);
        } else if (name == "api") {
            state->test.profile = test::piglit::strToProfile(pythonStr(value));
        } else if (name == "shader_version") {
            state->test.shader_version = parseVersion(value);
        } else if (name == "api_version") {
            state->test.api_version = parseVersion(value);
        } else if (name == "extensions") {
            state->test.extensions = pythonSet(value);
        } else if (name == "type_") {
            state->test.asm_type = pythonStr(value);
        } else if (name == "filename") {
            state->test.filename = pythonStr(value);
        } else if (name == "require_platforms") {
            state->test.require_platforms = pythonSequence(value);
        } else if (name == "exclude_platforms") {
            state->test.exclude_platforms = pythonSequence(value);
        }
        break;
    }
    }
}

void
endElementHandler(void * data, const char * name) {
    ParseState * state = (ParseState *)data;
    State stage = strToState(name);

    switch(stage) {
    case State::TEST: {
        bool valid = true;
        for (auto const & r : state->opts.exclude_filters) {
            valid = !std::regex_search(std::string{state->test.name}, r.regex);
            if (!valid) {
                break;
            }
        }
        if (valid) {
            for (auto const & r : state->opts.include_filters) {
                valid = std::regex_search(std::string{state->test.name}, r.regex);
                if (!valid) {
                    break;
                }
            }
        }

        if (valid) {
            std::unique_ptr<Group> group;
            std::unique_ptr<test::TestInterface> test;
            test::piglit::GLRequirements req{state->test.profile, state->test.shader_version,
                                             state->test.api_version, state->test.extensions};

            if (!state->test.exclude_platforms.empty()) {
                req.exclude_platforms = std::move(state->test.exclude_platforms);
            }
            if (!state->test.require_platforms.empty()) {
                req.require_platforms = std::move(state->test.require_platforms);
            }

            switch(state->test.type) {
            case Type::PIGLIT_GL: {
                group = std::make_unique<Group>(state->test.name);
                std::vector<std::string> cmd{state->test.command};
                cmd[0].insert(0, state->root + "bin/");
                test = std::make_unique<test::piglit::GLTest>(
                    cmd, state->test.concurrent, std::move(req), state->env);
                break;
            }
            case Type::PIGLIT_GL_BUILTIN_CONSTANTS: {
                std::vector<std::string> cmd(state->test.command);
                cmd[0].insert(0, state->root + "bin/");
                cmd.back().insert(0, state->root + "tests/");
                test = std::make_unique<test::piglit::GLTest>(
                    cmd, state->test.concurrent, std::move(req), state->env);
                group = std::make_unique<Group>(state->test.name);
                break;
            }
            case Type::GLSL_PARSER: {
                std::vector<std::string> cmd{state->test.command};
                cmd[0].insert(0, state->root + "bin/");
                cmd[1].insert(0, state->root);
                test = std::make_unique<test::piglit::GLSLParserTest>(
                    cmd, std::move(req), state->env);
                group = std::make_unique<Group>(state->test.name);
                break;
            }
            case Type::ASM_PARSER: {
                std::vector<std::string> cmd{
                    state->root + "bin/asmparsertest",
                    std::move(state->test.asm_type),
                    state->root + state->test.filename};
                test = std::make_unique<test::piglit::GLTest>(
                    cmd, std::move(req), state->env);
                group = std::make_unique<Group>(state->test.name);
                break;
            }
            case Type::SHADER_RUNNER: {
                std::vector<std::string> cmd{state->test.command};
                cmd[0].insert(0, state->root + "bin/");
                cmd[1].insert(0, state->root);
                test = std::make_unique<test::piglit::GLTest>(
                    cmd, std::move(req), state->env);
                group = std::make_unique<Group>(state->test.name);
                break;
            }
            case Type::VK_RUNNER: {
                std::vector<std::string> cmd{"vkrunner", state->root + state->test.filename};
                test = std::make_unique<test::piglit::VKTest>(cmd, state->env);
                group = std::make_unique<Group>(state->test.name);
                break;
            }
            default:
                UNREACHABLE("Unknown test type");
            }

            state->queue->put(testPair(std::move(group), std::move(test)));
        }
        state->test = TestData{};
        break;
    }
    case State::OPTION:
    case State::TESTLIST:
        break;
    default:
        UNREACHABLE(std::string{"Unknown stage "} + name);
    }
}

class MetaData {
public:
    std::list<std::string> text;
    std::list<std::string> files;
    const std::string root;
    bool in_profile = false;
    MetaData(const std::string r) : text{}, files{}, root{r} {};
    ~MetaData() {};
};

void
startMetaElementHandler(void * data, const char * name, const char ** attrs UNUSED) {
    auto meta = (MetaData *)data;
    std::string tag{name};

    if (tag == "Profile") {
        meta->in_profile = true;
    }
}

void
endMetaElementHandler(void * data, const char * name) {
    auto meta = (MetaData *)data;
    std::string tag{name};

    if (tag == "Profile") {
        meta->in_profile = false;
        if (!meta->text.empty()) {
            std::string profile = utils::joinStrings(meta->text.begin(), meta->text.end(), "");

            std::string f = meta->root + "tests/" + profile + ".xml.gz";
            if (utils::fileExists(f)) {
                meta->files.push_back(f);
                meta->text.clear();
                return;
            }

            f = meta->root + "tests/" + profile + ".xml";
            if (utils::fileExists(f)) {
                meta->files.push_back(f);
                meta->text.clear();
                return;
            }
        }
    }
}

void
characterMetaHandler(void * data, const char * text, int len) {
    auto meta = (MetaData *)data;
    if (meta->in_profile) {
        std::string str{text, (unsigned)len};
        meta->text.push_back(str);
    }
}

void inline
parsePlainXML(utils::XMLParser p, std::string f) {
    char data[65535];
    std::streamsize num_read = 0;
    std::ifstream ifile{f};

    do {
        ifile.read(data, sizeof(data));
        num_read = ifile.gcount();
        (void)p.parse((char *)data, num_read);
    } while (!ifile.eof());
}

void inline
parseGzipXML(utils::XMLParser p, std::string f) {
    char data[65535];
    std::streamsize num_read = 0;
    zstr::ifstream ifile{f};

    do {
        ifile.read(data, sizeof(data));
        num_read = ifile.gcount();
        (void)p.parse((char *)data, num_read);
    } while (!ifile.eof());
}

std::string
getRoot(std::string file) {
    const char * envroot = getenv("PIGLIT_SOURCE_DIR");
    if (envroot != NULL) {
        std::string root{envroot};
        if (root.back() != '/') {
            root.push_back('/');
        }
        return root;
    }

    std::string::size_type n;
    std::string root;
    n = file.rfind("/");
    root = file.substr(0, n - 1);
    n = root.rfind("/");
    root = root.substr(0, n + 1);
    return root;
}

std::list<std::string>
parseMetaXML(const std::vector<std::string> & files) {
    std::string root = getRoot(files[0]);
    std::list<std::string> rfiles{};
    utils::XMLParser p{startMetaElementHandler, endMetaElementHandler, characterMetaHandler};

    for (auto const & f : files) {
        if (f.substr(f.size() - 9, f.size()) == ".meta.xml") {
            MetaData state{root};
            p.userdata = (void *)&state;
            parsePlainXML(p, f);

            for (auto const & g : state.files) {
                rfiles.push_back(g);
            }
        } else {
            rfiles.push_back(f);
        }
    }

    return rfiles;
}

}

std::unique_ptr<unsigned long>
countTests(const Options & opts) {
    Counter counter{opts};
    std::list<std::string> rfiles = parseMetaXML(opts.profiles);

    utils::XMLParser p{countStartElementHandler, countEndElementHandler, (void *)&counter};
    for (auto const & f : rfiles) {
        // TODO: meta xml files need to be loaded before this
        if (f.substr(f.size() - 4, f.size()) == ".xml") {
            parsePlainXML(p, f);
        } else if (f.substr(f.size() - 7, f.size()) == ".xml.gz") {
            parseGzipXML(p, f);
        } else {
            UNREACHABLE("Unable to parse profile " + f);
        }
    }

    auto ptr = std::make_unique<unsigned long>(counter.count);
    return ptr;
}

void
readProfiles(std::shared_ptr<testQueue> queue, const Options & opts) {
    std::string root = getRoot(opts.profiles[0]);

    std::map<std::string, std::string> env{};
    unsigned i = 0;
    while (environ[i] != NULL) {
        std::string s{environ[i]};
        auto index = s.find("=");
        auto key = s.substr(0, index);
        auto value = s.substr(index + 1, s.size());
        env[key] = value;
        i++;
    }
    // opts.platform read the environment variable anyway, so this is correct
    // in both cases.
    env["PIGLIT_PLATFORM"] = opts.platform;

    std::list<std::string> files = parseMetaXML(opts.profiles);
    ParseState state = {queue, root, std::string{}, env, opts, TestData{}};
    utils::XMLParser p{startElementHandler, endElementHandler, (void *)&state};
    for (auto const & f : files) {
        if (f.substr(f.size() - 4, f.size()) == ".xml") {
            parsePlainXML(p, f);
        } else if (f.substr(f.size() - 7, f.size()) == ".xml.gz") {
            parseGzipXML(p, f);
        } else {
            UNREACHABLE("Unable to parse profile " + f);
        }
    }

    queue->close();
}

}
