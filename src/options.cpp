
/* Copyright © 2018 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <iostream>
#include <cstdlib>
#include "getopt.h"

#include "options.hpp"
#include "utils.hpp"

namespace swineherd {

namespace {

bool
isValidPlatform(const char * c) {
    std::string s(c);
    return (s == "gbm" or s == "glx" or s == "x11_egl" or s =="mixed_glx_egl" or s == "wayland");
}

static const std::string help_message = 
R"EOF(Usage:
    swineherd <result directory> <run name> <profile> [profile(s)] [options]

Description:
    A test runner written in C++.

Options:
    -h, --help
        Display this message and exit.
    -j, --jobs <N>
        The number of runner threads to use. Default 1
    -p, --platform <Platform>
        The window system to use.
    -t, --timeout <N>
        A test timeout in seconds.
    -x, --exclude-filter <regex>
        A regular expression (as a string) of test names to not run. May be used more than once.
    -i, --include-filter <regex>
        A regular expression (as a string) of test names to exclusively run. May be used more than once.
)EOF";

}

void
Options::parse_args(int argc, char **argv) {
    const char * short_opts = "hj:p:t:i:x:";
    const option long_opts[] = {
        { "help",           no_argument,       NULL, 'h' },
        { "jobs",           required_argument, NULL, 'j' },
        { "platform",       required_argument, NULL, 'p' },
        { "timeout",        required_argument, NULL, 't' },
        { "exclude-filter", required_argument, NULL, 'x' },
        { "include-filter", required_argument, NULL, 'i' },
        { 0 }
    };

    int c;
    while ((c = getopt_long(argc, argv, short_opts, long_opts, NULL)) != -1) {
        switch (c) {
        case 'j':
            try {
                threads = std::stoi(optarg);
            } catch (std::invalid_argument & e) {
                throw OptionException{"Invalid value for jobs \"" + std::string{optarg} + "\""};
            }
            break;
        case 'p':
            if (not isValidPlatform(optarg)) {
                throw OptionException{"Invalid platform \"" + std::string{optarg} + "\""};
            }
            platform = std::string{optarg};
            break;
        case 't':
            try {
                timeout = std::stoi(optarg);
            } catch (std::invalid_argument & e) {
                throw OptionException{"Invalid value for timeout \"" + std::string{optarg} + "\""};
            }
            break;
        // TODO: there's some deduplication to happen here
        case 'i':
            try {
                // TODO: these are going to be long lived, so maybe they should be heap allocated?
                std::string s{optarg};
                std::regex re{s, std::regex_constants::icase | std::regex_constants::optimize | std::regex_constants::extended};
                Regex r{s, re};
                include_filters.push_back(r);
            } catch (const std::regex_error & e) {
                throw OptionException{"Invalid regular expression: " + std::string{e.what()}};
            }
            break;
        case 'x':
            try {
                // TODO: these are going to be long lived, so maybe they should be heap allocated?
                std::string s{optarg};
                std::regex re{s, std::regex_constants::icase | std::regex_constants::optimize | std::regex_constants::extended};
                Regex r{s, re};
                exclude_filters.push_back(r);
            } catch (const std::regex_error & e) {
                throw OptionException{"Invalid regular expression: " + std::string{e.what()}};
            }
            break;
        case 'h':
        default:
            std::cout << help_message;
            exit(0);
        }
    }

    int i = optind;
    if (i >= argc) {
        throw OptionException{"Missing positional argument \"resultdir\""};
    }
    resultdir = argv[i++];

    if (i >= argc) {
        throw OptionException{"Missing positional argument \"name\""};
    }
    name = argv[i++];

    if (i >=  argc) {
        throw OptionException{"Missing positional argument(s) \"profiles\""};
    }
    profiles = std::vector<std::string>{argv + i, argv + argc};
    for (auto const & p : profiles) {
        if (not utils::fileExists(p)) {
            throw OptionException{"Cannot find a profile file: \"" + p + "\""};
        }
    }

    if (platform == "") {
        const char * p = getenv("PIGLIT_PLATFORM");
        if (p) {
            platform = std::string{p};
            if (not isValidPlatform(p)) {
                throw OptionException{"Invalid platform \"" + platform + "\""};
            }
        } else {
            platform = "gbm";
        }
    }
}

}
