/* Copyright © 2018-2019 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <algorithm>
#include <array>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <cerrno>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/poll.h>
#include <sys/stat.h>

extern "C" char **environ;

#include "utils.hpp"

#define READ 0
#define WRITE 1

namespace swineherd {

namespace utils {

processOut
subProcess(std::vector<std::string> cmd, int timeout, const std::map<std::string, std::string> * env) {
    int out_pipes[2];
    int err_pipes[2];
    pipe(out_pipes);
    pipe(err_pipes);

    pid_t pid = fork();
    if (pid == 0) {
        dup2(out_pipes[WRITE], STDOUT_FILENO);
        dup2(err_pipes[WRITE], STDERR_FILENO);
        close(out_pipes[READ]);
        close(out_pipes[WRITE]);
        close(err_pipes[READ]);
        close(err_pipes[WRITE]);

        char * c_cmd[cmd.size() + 1];
        for (unsigned i = 0; i < cmd.size(); ++i) {
            c_cmd[i] = strdup(cmd[i].c_str());
        }
        c_cmd[cmd.size()] = NULL;

        if (env == nullptr) {
            execvp(c_cmd[0], c_cmd);
        } else {
            char * realenv[env->size()];
            unsigned i = 0;
            for (auto const & kv : *env) {
                realenv[i] = strdup((kv.first + "=" + kv.second).c_str());
                ++i;
            }
            environ = realenv;
            execvp(c_cmd[0], c_cmd);
        }
        std::cerr << "Program failed to execute: " << strerror(errno) << std::endl;
        _exit(127);
    }

    close(out_pipes[WRITE]);
    close(err_pipes[WRITE]);

    std::array<char, 16384> buffer{};
    std::string out, err;
    int status;
    int count = 0;

    std::array<pollfd, 2> fds;
    fds[0] = { out_pipes[READ], POLLIN, 0 };
    fds[1] = { err_pipes[READ], POLLIN, 0 };

    if (timeout != -1) {
        timeout *= 1000;
    }
    while (true) {
        int rt = poll(fds.data(), fds.size(), timeout);

        if (rt < 0) {
            std::cerr << "Error: " << strerror(errno) << std::endl;
        } else if (rt == 0) {
            kill(pid, SIGKILL);
            for (auto const & f : fds) {
                close(f.fd);
            }
            throw ProcessTimeout(out, err);
        }

        if (fds[0].revents & POLLIN) {
            count = read(out_pipes[READ], buffer.data(), buffer.size());
            out.append(buffer.begin(), buffer.begin() + count);
        }
        if (fds[1].revents & POLLIN) {
            count = read(err_pipes[READ], buffer.data(), buffer.size());
            err.append(buffer.begin(), buffer.begin() + count);
        }

        if (fds[0].revents & POLLHUP and fds[1].revents & POLLHUP) {
            break;
        }
    }

    for (auto const & f : fds) {
        close(f.fd);
    }

    while(waitpid(pid, &status, 0) == -1);

    if (status > 255) {
        status %= 255;
    }

    // On Unix-like OSes return codes > 128 are traditionally used for
    // returning error codes, 128 + n, where n is the code.
    if (status > 128) {
        status -= 128;
        status *= -1;
    }

    return processOut(out, err, status);
}

void
makedir(const std::string & directory) {
    static mode_t mode = S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH;
    int ok = mkdir(directory.c_str(), mode);
    if (ok == -1) {
        int err = errno;
        throw MkdirError(err);
    }
}

bool
fileExists(const std::string & name) {
    struct stat b;
    return (stat(name.c_str(), &b) == 0);
}

XMLParser::~XMLParser() {
    if (parser != NULL) {
        XML_ParserFree(parser);
    }
}

void
XMLParser::create_parser() {
    parser = XML_ParserCreate(NULL);
    if (start_element_handler != NULL or end_element_handler != NULL) {
        XML_SetElementHandler(parser, start_element_handler, end_element_handler);
    }
    if (character_handler != NULL) {
        XML_SetCharacterDataHandler(parser, character_handler);
    }
    if (userdata != NULL) {
        XML_SetUserData(parser, userdata);
    }
}

bool
XMLParser::parse(const char * str, int length) {
    if (!parser) {
        create_parser();
    }

    int fin = 0;
    XML_Parse(parser, str, length, fin);

    if (fin) {
        XML_ParserFree(parser);
        parser = NULL;
    }

    return fin != 0;
}

bool
XMLParser::parse(std::string str) {
    return parse(str.c_str(), str.length());
}

}

}
