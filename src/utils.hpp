/* Copyright © 2018-2019 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <string>
#include <tuple>
#include <vector>
#include <sstream>
#include <cstring>
#include <map>

#include <expat.h>

namespace swineherd {

namespace utils {

typedef std::tuple<std::string, std::string, int> processOut;

class ProcessTimeout : public std::exception {
public:
    ProcessTimeout(const std::string out, const std::string err) : out{out}, err{err} {};
    const char * what() const throw() {
        return "Test timed out";
    }
    std::string out;
    std::string err;
};

template <typename Iter>
std::string
joinStrings(Iter it, Iter end, const char * delim) {
    std::string out = *it;
    ++it;
    for (; it != end; ++it) {
        out += delim;
        out += *it;
    }
    return out;
}

template <typename Out>
void split(const std::string &s, char delim, Out result) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        *(result++) = item;
    }
}

processOut subProcess(std::vector<std::string>, int = -1,
                      const std::map<std::string, std::string> * = nullptr);

class MkdirError : public std::exception {
public:
    MkdirError(const int err) : err{err} {};
    const char * what() const throw() {
        return strerror(err);
    }
    int err;
};

void makedir(const std::string &);
bool fileExists(const std::string &);

class XMLParser {
private:
    XML_Parser parser = NULL;
    void create_parser();
    XML_StartElementHandler start_element_handler = NULL;
    XML_EndElementHandler end_element_handler = NULL;
    XML_CharacterDataHandler character_handler = NULL;
public:
    void * userdata = NULL;

    XMLParser() {};
    XMLParser(XML_StartElementHandler s, XML_EndElementHandler e, XML_CharacterDataHandler c = NULL) :
        start_element_handler{s}, end_element_handler{e}, character_handler{c} {};
    XMLParser(XML_StartElementHandler s, XML_EndElementHandler e, XML_CharacterDataHandler c, void * u) :
        start_element_handler{s}, end_element_handler{e}, character_handler{c}, userdata{u} {};
    XMLParser(XML_StartElementHandler s, XML_EndElementHandler e, void * u) :
        start_element_handler{s}, end_element_handler{e}, userdata{u} {};
    ~XMLParser();
    bool parse(const char * str, int length);
    bool parse(std::string str);
};

}

}
