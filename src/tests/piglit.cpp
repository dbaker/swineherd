/* Copyright © 2018 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <algorithm>
#include <cassert>
#include <vector>
#include <sstream>
#include <iostream>
#include <list>

#include <json/json.h>

#include "config.h"
#include "utils.hpp"
#include "tests/piglit.hpp"
#include "tests/piglit_waffle.hpp"

namespace swineherd {

namespace test {

namespace piglit {

void
BaseTest::runCmd(Result & result, int timeout) {
    try {
        std::tie(result.out, result.err, result.returncode) =
            utils::subProcess(result.command, timeout, &env);
    } catch (utils::ProcessTimeout & e) {
        result.status[""] = Status::TIMEOUT;
        result.out = e.out;
        result.err = e.err;
        return;
    }

    // TODO: There is a bit more to this
    if (result.returncode < 0) {
        result.status[""] = Status::CRASH;
    } else if (result.returncode > 0) {
        result.status[""] = Status::FAIL;
    }
}

void
BaseTest::interpretResult(Result & result) {
    if (result.returncode == 127) {
        result.status[""] = Status::SKIP;
        return;
    }

    const std::string prefix = "PIGLIT: ";
    std::istringstream lines{result.out};
    std::ostringstream out;
    std::string line;
    Statuses subtests;
    Json::CharReaderBuilder rbuilder;
    Json::CharReader * reader = rbuilder.newCharReader();

    while (getline(lines, line)) {
        if (line.substr(0, prefix.size()) == prefix) {
            line = line.substr(prefix.length());
            Json::Value root;
            std::string errs;

            bool ok = reader->parse(line.c_str(), line.c_str() + line.size(), &root, &errs);
            if (!ok) {
                // XXX: handle
                abort();
            }
            if (root.isMember("result")) {
                std::string r = root["result"].asString();
                result.status[""] = strToStatus(&r);
            } else if (root.isMember("enumerate subtests")) {
                const Json::Value subtests = root["enumerate subtests"];
                for (auto const& v : subtests) {
                    result.status[v.asString()] = Status::NOTRUN;
                }
            } else if (root.isMember("subtest")) {
                const Json::Value subtests = root["subtest"];
                std::vector<std::string> names = subtests.getMemberNames();
                for (auto const& key : names) {
                    auto stat = subtests[key].asString();
                    result.status[key] = strToStatus(&stat);
                }
            } else {
                UNREACHABLE("No result in test output.");
            };
        } else {
            out << line << '\n';
        }
    }
    delete reader;

    result.out.assign(out.str());

    return;
}

std::unique_ptr<Result>
GLTest::runTest(const Options & opts, const SystemInfo & info) {
    auto result = std::make_unique<Result>();

    std::vector<std::string> cmd{command};
    cmd.push_back("-auto");
    if (concurrent) {
        cmd.push_back("-fbo");
    }
    result->command = std::move(cmd);

    detectSkip(*result, opts, info);
    if (result->status[""] == Status::SKIP) {
        return result;
    }

    runCmd(*result, opts.timeout);
    if (result->status[""] == Status::CRASH) {
        return result;
    }

    interpretResult(*result);

#ifndef NDEBUG
    if (result->getStatus() == Status::NOTRUN) {
        std::cerr << "NOTRUN: " << result->command[0] << std::endl;
    }
#endif

    return result;
}

void
GLTest::detectSkip(Result & result, const Options & opts, const SystemInfo & info) {
    if (!requirements.require_platforms.empty()) {
        if (std::find(requirements.require_platforms.begin(),
                      requirements.require_platforms.end(), opts.platform) ==
                requirements.require_platforms.end()) {
            result.status[""] = Status::SKIP;
            result.out = "Skipped due to incompatible platform";
            return;
        }
    }
    if (!requirements.exclude_platforms.empty()) {
        if (std::find(requirements.exclude_platforms.begin(),
                      requirements.exclude_platforms.end(), opts.platform) !=
                requirements.exclude_platforms.end()) {
            result.status[""] = Status::SKIP;
            result.out = "Skipped due to incompatible platform";
            return;
        }
    }

    auto search = info.opengl.find(requirements.profile);
    if (search == info.opengl.end()) {
        std::cerr << to_string(requirements.profile) << std::endl;
        abort();
    }
    const ProfileInfo profile = search->second;

    if (requirements.api_version and profile.lang_version < requirements.api_version) {
        result.status[""] = Status::SKIP;
        result.out = "Fast skipped. Requires API " + to_string(requirements.profile) +
                     ", version " + std::string(requirements.api_version) + ".\n";
        return;
    }
    if (requirements.shader_version and
            profile.shader_version < requirements.shader_version) {
        result.status[""] = Status::SKIP;
        result.out = "Fast skipped. Requires API " + to_string(requirements.profile) +
                     ", shader language version " +
                     std::string(requirements.shader_version) + ".\n";
        return;
    }
    if (!requirements.extensions.empty()) {
        std::list<std::string> missing{};
        for (auto const & e : requirements.extensions) {
            if (profile.extensions.count(e) == 0) {
                missing.push_back(e);
            }
        }
        if (!missing.empty()) {
            result.status[""] = Status::SKIP;
            result.out = "Fast skipped. Requires the following unsupported extensions " +
                         utils::joinStrings(missing.begin(), missing.end(), ", ") + ".\n";
            return;
        }
    }
}

std::unique_ptr<Result>
GLSLParserTest::runTest(const Options & opts, const SystemInfo & info) {
    auto result = std::make_unique<Result>();
    result->command = command;

    detectSkip(*result, opts, info);
    if (result->status[""] == Status::SKIP) {
        return result;
    }

    runCmd(*result, opts.timeout);
    if (result->status[""] == Status::CRASH) {
        return result;
    }

    interpretResult(*result);

    return result;
}

std::unique_ptr<Result>
VKTest::runTest(const Options & opts, UNUSED const SystemInfo & info) {
    auto result = std::make_unique<Result>();
    result->command = command;

    runCmd(*result, opts.timeout);
    if (result->status[""] == Status::CRASH) {
        return result;
    }

    interpretResult(*result);

    return result;
}

}

}

}
