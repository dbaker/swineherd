/* Copyright © 2018 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <list>
#include <string>
#include <vector>
#include <map>
#include <unordered_set>
#include <array>

#include "options.hpp"
#include "version.hpp"
#include "tests/base.hpp"
#include "tests/piglit_waffle.hpp"

namespace swineherd {

namespace test {

namespace piglit {

namespace {

typedef std::map<std::string, std::string> EnvMap;

}

class GLRequirements {
public:
    GLRequirements() : profile{Profile::CORE}, shader_version{0, 0}, api_version{0, 0}, extensions{} {};
    GLRequirements(Profile p, std::array<unsigned short, 2> sv, std::array<unsigned short, 2> av, std::unordered_set<std::string> e) :
        profile{p}, shader_version{sv}, api_version{av}, extensions{e} {};
    ~GLRequirements() {};

    std::vector<std::string> require_platforms{};
    std::vector<std::string> exclude_platforms{};
    Profile profile;
    Version shader_version;
    Version api_version;
    std::unordered_set<std::string> extensions;
};

class BaseTest : public TestInterface {
public:
    BaseTest() = delete;
protected:
    BaseTest(std::vector<std::string> cmd, EnvMap e) :
        TestInterface{true}, command{cmd}, env{e} {};
    BaseTest(std::vector<std::string> cmd, bool concurrent, EnvMap e) :
        TestInterface{concurrent}, command{cmd}, env{e} {};
    ~BaseTest() {};
    void interpretResult(Result &);
    void runCmd(Result &, int);
    const std::vector<std::string> command;
    const EnvMap env;
};

class GLTest : public BaseTest {
public:
    GLTest(std::vector<std::string> cmd, GLRequirements r, EnvMap e) :
        BaseTest(cmd, true, e), requirements{r} {};
    GLTest(std::vector<std::string> cmd, bool con, GLRequirements r, EnvMap e) :
        BaseTest(cmd, con, e), requirements{r} {};
    ~GLTest() {};
    std::unique_ptr<Result> runTest(const Options &, const SystemInfo &);
    void detectSkip(Result &, const Options &, const SystemInfo &);

    GLRequirements requirements{};
};

class GLSLParserTest : public GLTest {
public:
    GLSLParserTest(std::vector<std::string> cmd, GLRequirements r, EnvMap e) :
        GLTest(cmd, true, r, e) {};
    ~GLSLParserTest() {};
    std::unique_ptr<Result> runTest(const Options &, const SystemInfo &);
};

class VKTest : public BaseTest {
public:
    VKTest(std::vector<std::string> cmd, EnvMap e) : BaseTest(cmd, true, e) {};
    ~VKTest() {};
    std::unique_ptr<Result> runTest(const Options &, const SystemInfo &);
};

}

}

}
