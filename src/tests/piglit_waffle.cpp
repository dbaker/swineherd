/* Copyright © 2018 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <vector>
#include <cstring>
#include <iterator>
#include <iostream>
#include <string>
#include <sstream>

#include <waffle-1/waffle.h>

#include "config.h"
#include "piglit_waffle.hpp"
#include "utils.hpp"

namespace swineherd {

namespace test {

namespace piglit {

namespace {

// XXX: mixed_x11_egl?
waffle_enum platformToEnum(std::string platform) {
    if (platform == "glx") {
        return WAFFLE_PLATFORM_GLX;
    } else if (platform == "wayland") {
        return WAFFLE_PLATFORM_WAYLAND;
    } else if (platform == "x11_egl") {
        return WAFFLE_PLATFORM_X11_EGL;
    } else if (platform == "gbm") {
        return WAFFLE_PLATFORM_GBM;
    }
    UNREACHABLE("Unknown platform " + platform);
}

waffle_enum waffleProfile(Profile p) {
    switch (p) {
    case Profile::CORE:
        return WAFFLE_CONTEXT_CORE_PROFILE;
    case Profile::COMPAT:
        return WAFFLE_CONTEXT_COMPATIBILITY_PROFILE;
    case Profile::ES1:
    case Profile::ES2:
        return WAFFLE_NONE;
    }
    UNREACHABLE("Unknown waffle profile");
}

waffle_enum waffleAPI(Profile p) {
    switch (p) {
    case Profile::CORE:
    case Profile::COMPAT:
        return WAFFLE_CONTEXT_OPENGL;
    case Profile::ES1:
        return WAFFLE_CONTEXT_OPENGL_ES1;
    case Profile::ES2:
        return WAFFLE_CONTEXT_OPENGL_ES2;
    }
    UNREACHABLE("Unknown waffle api");
}

/* Everything following this comment in this namspace has been taken from
 * waffle's wflinfo (with adaptations), and is covered by the following
 * license:
 *
 * Copyright 2014 Intel Corporation
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

typedef float GLclampf;
typedef unsigned int GLbitfield;
typedef unsigned int GLint;
typedef int GLsizei;
typedef unsigned int GLenum;
typedef void GLvoid;
typedef unsigned char GLubyte;

enum {
    // Copied from <GL/gl*.h>.
    GL_NO_ERROR = 0,

    GL_CONTEXT_FLAGS = 0x821e,
    GL_CONTEXT_FLAG_FORWARD_COMPATIBLE_BIT = 0x00000001,
    GL_CONTEXT_FLAG_DEBUG_BIT              = 0x00000002,
    GL_CONTEXT_FLAG_ROBUST_ACCESS_BIT_ARB  = 0x00000004,

    GL_VENDOR                              = 0x1F00,
    GL_RENDERER                            = 0x1F01,
    GL_VERSION                             = 0x1F02,
    GL_EXTENSIONS                          = 0x1F03,
    GL_NUM_EXTENSIONS                      = 0x821D,
    GL_SHADING_LANGUAGE_VERSION            = 0x8B8C,
};

#define WINDOW_WIDTH  320
#define WINDOW_HEIGHT 240

#define GL_MAJOR_VERSION                  0x821B
#define GL_MINOR_VERSION                  0x821C
#define GL_CONTEXT_PROFILE_MASK           0x9126
#define GL_CONTEXT_CORE_PROFILE_BIT       0x00000001
#define GL_CONTEXT_COMPATIBILITY_PROFILE_BIT 0x00000002

#ifndef _WIN32
#define APIENTRY
#else
#ifndef APIENTRY
#define APIENTRY __stdcall
#endif
#endif

static GLenum (APIENTRY *glGetError)(void);
static void (APIENTRY *glGetIntegerv)(GLenum pname, GLint *params);
static const GLubyte * (APIENTRY *glGetString)(GLenum name);
static const GLubyte * (APIENTRY *glGetStringi)(GLenum name, GLint i);

struct ConfigAttrs {
public:
    enum waffle_enum api;
    enum waffle_enum profile;
    int32_t major = WAFFLE_DONT_CARE;
    int32_t minor = WAFFLE_DONT_CARE;
    ConfigAttrs(waffle_enum a, waffle_enum p) : api{a}, profile{p} {};
    ~ConfigAttrs() {};
};

bool
strneq(const char *a, const char *b, size_t n) {
    return strncmp(a, b, n) == 0;
}

int
gl_get_version(void) {
    GLint major_version = 0;
    GLint minor_version = 0;

    glGetIntegerv(GL_MAJOR_VERSION, &major_version);
    if (glGetError()) {
        abort();
    }

    glGetIntegerv(GL_MINOR_VERSION, &minor_version);
    if (glGetError()) {
        abort();
    }
    return 10 * major_version + minor_version;
}

bool
gl_has_extension_GetString(const char *name) {
#define BUF_LEN 4096
    char exts[BUF_LEN];

    const uint8_t *exts_orig = glGetString(GL_EXTENSIONS);
    if (glGetError()) {
        abort();
    }

    memcpy(exts, exts_orig, BUF_LEN);
    exts[BUF_LEN - 1] = 0;

    char *ext = strtok(exts, " ");
    do {
        if (strneq(ext, name, BUF_LEN)) {
            return true;
        }
        ext = strtok(NULL, " ");
    } while (ext);

    return false;
#undef BUF_LEN
}

bool
gl_has_extension_GetStringi(const char *name) {
    const size_t max_ext_len = 128;
    GLint num_exts = 0;

    glGetIntegerv(GL_NUM_EXTENSIONS, &num_exts);
    if (glGetError()) {
        abort();
    }

    for (GLint i = 0; i < num_exts; i++) {
        const uint8_t *ext = glGetStringi(GL_EXTENSIONS, i);
        if (!ext || glGetError()) {
            abort();
        } else if (strneq((const char*) ext, name, max_ext_len)) {
            return true;
        }
    }

    return false;
}

bool
gl_has_extension(const char *name) {
    if (gl_get_version() >= 30) {
        return gl_has_extension_GetStringi(name);
    } else {
        return gl_has_extension_GetString(name);
    }
}

enum waffle_enum
gl_get_profile(void)
{
    int version = gl_get_version();

    if (version >= 32) {
        GLint profile_mask = 0;
        glGetIntegerv(GL_CONTEXT_PROFILE_MASK, &profile_mask);
        if (glGetError()) {
            abort();
        } else if (profile_mask & GL_CONTEXT_CORE_PROFILE_BIT) {
            return WAFFLE_CONTEXT_CORE_PROFILE;
        } else if (profile_mask & GL_CONTEXT_COMPATIBILITY_PROFILE_BIT) {
            return WAFFLE_CONTEXT_COMPATIBILITY_PROFILE;
        } else {
            abort();
        }
    } else if (version == 31) {
        if (gl_has_extension("GL_ARB_compatibility")) {
            return WAFFLE_CONTEXT_CORE_PROFILE;
        } else {
            return WAFFLE_CONTEXT_COMPATIBILITY_PROFILE;
        }
    } else {
        return WAFFLE_NONE;
    }
}

bool
try_create_context(waffle_display *dpy,
                   ConfigAttrs attrs,
                   waffle_context **out_ctx,
                   waffle_config **out_config)
{
    int i;
    int32_t config_attrib_list[64];
    struct waffle_context *ctx = NULL;
    struct waffle_config *config = NULL;

    i = 0;
    config_attrib_list[i++] = WAFFLE_CONTEXT_API;
    config_attrib_list[i++] = attrs.api;
    config_attrib_list[i++] = WAFFLE_CONTEXT_PROFILE;
    config_attrib_list[i++] = attrs.profile;

    if (attrs.major != WAFFLE_DONT_CARE && attrs.minor != WAFFLE_DONT_CARE) {
        config_attrib_list[i++] = WAFFLE_CONTEXT_MAJOR_VERSION;
        config_attrib_list[i++] = attrs.major;
        config_attrib_list[i++] = WAFFLE_CONTEXT_MINOR_VERSION;
        config_attrib_list[i++] = attrs.minor;
    }

    static int32_t dont_care_attribs[] = {
        WAFFLE_RED_SIZE,
        WAFFLE_GREEN_SIZE,
        WAFFLE_BLUE_SIZE,
        WAFFLE_ALPHA_SIZE,
        WAFFLE_DEPTH_SIZE,
        WAFFLE_STENCIL_SIZE,
        WAFFLE_DOUBLE_BUFFERED,
    };
    int dont_care_attribs_count =
        sizeof(dont_care_attribs) / sizeof(dont_care_attribs[0]);

    for (int j = 0; j < dont_care_attribs_count; j++) {
        config_attrib_list[i++] = dont_care_attribs[j];
        config_attrib_list[i++] = WAFFLE_DONT_CARE;
    }

    config_attrib_list[i++] = 0;

    config = waffle_config_choose(dpy, config_attrib_list);
    if (!config) {
        goto fail;
    }

    ctx = waffle_context_create(config, NULL);
    if (!ctx) {
        goto fail;
    }

    *out_ctx = ctx;
    *out_config = config;
    return true;

fail:
    if (ctx) {
        waffle_context_destroy(ctx);
    }
    if (config) {
        waffle_config_destroy(config);
    }

    return false;
}

bool
try_create_context_gl31(waffle_display *dpy,
                        ConfigAttrs attrs,
                        waffle_context **out_ctx,
                        waffle_config **out_config)
{
    struct waffle_config *config = NULL;
    struct waffle_context *ctx = NULL;
    bool ok;

    // It's illegal to request a waffle_config with WAFFLE_CONTEXT_PROFILE
    // != WAFFLE_NONE. Therefore, request an OpenGL 3.1 config without
    // a profile and later verify that the desired and actual profile
    // agree.
    const enum waffle_enum desired_profile = attrs.profile;
    attrs.major = 3;
    attrs.minor = 1;
    attrs.profile = WAFFLE_NONE;
    try_create_context(dpy, attrs, &ctx, &config);

    if (desired_profile == WAFFLE_NONE || desired_profile == WAFFLE_DONT_CARE) {
        *out_ctx = ctx;
        *out_config = config;
        return true;
    }

    // The user cares about the profile. We must bind the context to inspect
    // its profile.
    //
    // Skip window creation. No window is needed when binding an OpenGL >= 3.0
    // context.
    ok = waffle_make_current(dpy, NULL, ctx);
    if (!ok) {
        abort();
    }

    const enum waffle_enum actual_profile = gl_get_profile();
    waffle_make_current(dpy, NULL, NULL);
    if (actual_profile == desired_profile) {
        *out_ctx = ctx;
        *out_config = config;
        return true;
    }

    return false;
}

bool
create_context(waffle_display *dpy,
               ConfigAttrs attrs,
               waffle_context **out_ctx,
               waffle_config **out_config)
{
    bool ok = false;

    if (attrs.api == WAFFLE_CONTEXT_OPENGL && attrs.profile != WAFFLE_NONE) {
        // If the user requested OpenGL and a CORE or COMPAT profile,
        // but they didn't specify a version, then we'll try a set
        // of known versions from highest to lowest.

        static std::vector<int> known_gl_profile_versions{32, 33, 40, 41, 42, 43, 44, 45, 46};

        for (auto const & v : known_gl_profile_versions) {
            attrs.major = v / 10;
            attrs.minor = v % 10;
            ok = try_create_context(dpy, attrs, out_ctx, out_config);
            if (ok) {
                return ok;
            }
        }

        // Handle OpenGL 3.1 separately because profiles are weird in 3.1.
        ok = try_create_context_gl31(dpy, attrs, out_ctx, out_config);
        return ok;
    } else {
        return try_create_context(dpy, attrs, out_ctx, out_config);
    }
}

Version
get_version() {
    const char *version_str = (const char *) glGetString(GL_VERSION);
    if (glGetError() != GL_NO_ERROR || version_str == NULL) {
        abort();
    }
    std::string str{version_str};
    std::vector<std::string> split{};
    utils::split(str, ' ', std::back_inserter(split));
    if (split[0] == "OpenGL") {
        str = split[2];
    } else {
        str = split[0];
    }

    return Version(str);
}

std::unordered_set<std::string>
get_extensions(bool use_stringi) {
    std::unordered_set<std::string> extensions{};
    if (use_stringi) {
        std::string ext;
        unsigned int count;

        glGetIntegerv(GL_NUM_EXTENSIONS, &count);
        if (glGetError() != GL_NO_ERROR) {
            abort();
        }

        for (unsigned int i = 0; i < count; i++) {
            ext = std::string{(const char *)glGetStringi(GL_EXTENSIONS, i)};
            if (glGetError() != GL_NO_ERROR) {
                abort();
            }
            extensions.insert(ext);
        }
    } else {
        std::string s{(const char *)glGetString(GL_EXTENSIONS)};
        std::stringstream ss{s};
        std::string e;
        while (std::getline(ss, e, ' ')) {
            extensions.insert(e);
        }
    }

    return extensions;
}

ProfileInfo makeInfo(std::string platform, Profile profile) {
    int32_t init_attrib_list[3];
    init_attrib_list[0] = WAFFLE_PLATFORM;
    init_attrib_list[1] = platformToEnum(platform);
    init_attrib_list[2] = WAFFLE_NONE;
    bool ok;

    waffle_display * dpy;
    waffle_config * config;
    waffle_context * ctx;
    waffle_window * window;

    ok = waffle_init(init_attrib_list);
    if (!ok) {
#ifdef DEBUG
        std::cerr << "Failed to initialized waffle" << std::endl;
#endif
        return ProfileInfo{};
    }
    dpy = waffle_display_connect(NULL);

    waffle_enum dl;
    switch(profile) {
    case Profile::CORE:
    case Profile::COMPAT:
        dl = WAFFLE_DL_OPENGL;
        break;
    case Profile::ES1:
        dl = WAFFLE_DL_OPENGL_ES1;
        break;
    case Profile::ES2:
        dl = WAFFLE_DL_OPENGL_ES2;
        break;
    default:
        UNREACHABLE("Unknown waffle profile");
    }

    glGetError = (GLenum (*)(void))waffle_dl_sym(dl, "glGetError");
    glGetIntegerv = (void (*)(GLenum, GLint *))waffle_dl_sym(dl, "glGetIntegerv");
    glGetString = (const GLubyte * (*)(GLenum))waffle_dl_sym(dl, "glGetString");
    glGetStringi = (const GLubyte * (*)(GLenum, GLint))waffle_dl_sym(dl, "glGetStringi");
    if (!glGetStringi) {
        glGetStringi = (const GLubyte * (*)(GLenum, GLint))waffle_get_proc_address("glGetStringi");
    }

    ConfigAttrs conf{waffleAPI(profile), waffleProfile(profile)};
    ok = create_context(dpy, conf, &ctx, &config);
    if (!ok) {
        if (profile == Profile::COMPAT) {
            // In this case try to create a "legacy" pre-profiles context.
            conf = ConfigAttrs{waffleAPI(profile), WAFFLE_NONE};
            ok = create_context(dpy, conf, &ctx, &config);
        }
        if (!ok) {
            return ProfileInfo{};
        }
    }
    window = waffle_window_create(config, WINDOW_WIDTH, WINDOW_HEIGHT);
    waffle_make_current(dpy, window, ctx);

    while (glGetError() != GL_NO_ERROR) {
        /* Clear all errors */
    }

    Version version = get_version();
    Version shader{0, 0};
    if (((profile == Profile::CORE || profile == Profile::COMPAT) && version >= Version{2, 0})
            || profile == Profile::ES2) {
        const char * ver = (const char *)glGetString(GL_SHADING_LANGUAGE_VERSION);
        std::string str{ver};
        if (profile == Profile::ES2) {
            std::vector<std::string> split{};
            utils::split(str, ' ', std::back_inserter(split));
            str = split[4];
        }
        shader = Version(str);
    }

    ProfileInfo info{version, shader, get_extensions(version >= Version{3, 1})};

    waffle_make_current(dpy, NULL, NULL);
    waffle_window_destroy(window);
    waffle_context_destroy(ctx);
    waffle_config_destroy(config);
    waffle_display_disconnect(dpy);
    //waffle_teardown();

    return info;
}

}

Profile
strToProfile(std::string api) {
    if (api == "core") {
        return Profile::CORE;
    } else if (api == "compat") {
        return Profile::COMPAT;
    } else if (api == "gles1") {
        return Profile::ES1;
    } else if (api == "gles2") {
        return Profile::ES2;
    } else if (api == "gles3") {
        return Profile::ES2;
    }
    abort();
}

std::string
to_string(Profile p) {
    switch(p) {
    case Profile::CORE:
        return "core";
    case Profile::COMPAT:
        return "compat";
    case Profile::ES1:
        return "gles1";
    case Profile::ES2:
        return "gles2";
    }
    abort();
}

OpenGLInfo makeOGLInfo(std::string platform) {
    OpenGLInfo info{};
    info.insert(std::make_pair(Profile::ES1, makeInfo(platform, Profile::ES1)));
    info.insert(std::make_pair(Profile::ES2, makeInfo(platform, Profile::ES2)));
    info.insert(std::make_pair(Profile::CORE, makeInfo(platform, Profile::CORE)));
    info.insert(std::make_pair(Profile::COMPAT, makeInfo(platform, Profile::COMPAT)));
    return info;
}

}

}

}
