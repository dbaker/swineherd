/* Copyright © 2018 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <unordered_set>
#include <string>
#include <map>

#include "version.hpp"

namespace swineherd {

namespace test {

namespace piglit {

enum class Profile {
    CORE,
    COMPAT,
    ES1,
    ES2,
};

Profile strToProfile(std::string);
std::string to_string(Profile);

class ProfileInfo {
public:
    const Version shader_version;
    const Version lang_version;
    const std::unordered_set<std::string> extensions;

    ProfileInfo(Version s, Version l, std::unordered_set<std::string> e) :
        shader_version{s}, lang_version{l}, extensions{e} {};
    ProfileInfo() : shader_version{0, 0}, lang_version{0, 0}, extensions{} {};
    ~ProfileInfo() {};
};

typedef std::map<Profile, const ProfileInfo> OpenGLInfo;
OpenGLInfo makeOGLInfo(std::string);

}

}

}
